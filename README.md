# QA Automation using Python #

Este projeto é uma prova de conceito para utilização e criação de testes de software automatizados utilizando Python.

### O que ter em mente antes de iniciar? ###

* IDE
* Pré requisitos

### IDE ###

* Para este projeto foi utilizada a IDE PyCharm

### Pré requisitos ###

* Instalação do Python
* Conhecimentos em Selenium
* Bla bla bla

### Para mais informações ###

* https://gist.github.com/diyan/5763644
* https://cleitonbueno.com/python-meu-primeiro-programa-em-python/
* https://github.com/atodorov/qa-automation-python-selenium-101

### Comandos importantes ###

* python -m unittest test_module1 test_module2
* python -m unittest test_module.TestClass
* python -m unittest test_module.TestClass.test_method